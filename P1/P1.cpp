// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "P1.h"
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_NONAME, szNonameWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_P1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassExW(&wcex);
	//Child Window(Noname)

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = NonameWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szNonameWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);
	return 1;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		initFrameWindow(hWnd);
		CreateToolBar(hWnd);
		return 0;
	}
	case WM_COMMAND:
		onCommand(hWnd, message, wParam, lParam);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_SIZE:
	{
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 25, w, h - 25, TRUE);
		MoveWindow(hToolBarWnd, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, TRUE);
		return 0;
	}
	case WM_NOTIFY:
	{
		LPTOOLTIPTEXT   lpToolTipText;
		TCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource

															// lParam: address of TOOLTIPTEXT struct
		lpToolTipText = (LPTOOLTIPTEXT)lParam;
		if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
		{
			// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
			LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);
			lpToolTipText->lpszText = szToolTipText;
		}
		return 0;
	}
	}
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}
LRESULT CALLBACK NonameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int type, x1, x2, y1, y2, x3, y3;
	static bool flag = false;
	static HWND string;
	switch (message)
	{
	case WM_COMMAND:
	{
		break;
	}

	case WM_CREATE:
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), currentitem, MF_CHECKED | MF_BYCOMMAND);
		InitialChildWnd(hWnd, type);
		break;
	}
	case WM_MOUSEMOVE:
	{
		if (currentitem == ID_DRAW_SELECTOBJECT)
		{
			CHILD_WND_DATA*wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			if (type1 != -1 && !wndData->objects.empty() && wndData->currentIndex != -1)
			{
				_Edit_OnMouseMove(hWnd, wParam, lParam, wndData->objects[wndData->currentIndex], type1);
				wndData->objects[wndData->currentIndex]->selected(hWnd);
			}
		}
		else
		{
			OnMouseMove(hWnd, wParam, lParam, x1, x2, y1, y2);
		}
		
		break;
	}
	case WM_LBUTTONDOWN:
	{
		if (currentitem == ID_DRAW_SELECTOBJECT)
		{
			CHILD_WND_DATA*wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			if(!wndData->objects.empty())
				_Edit_OnLButtonDown(hWnd, lParam, type1);
		}
		else
		{
			OnLButtonDown(hWnd, wParam, lParam, x1, x2, y1, y2);
		}
		
		break;
	}
	case WM_LBUTTONUP:
	{
		OnLButtonUp(hWnd, wParam, lParam, x1, x2, y1, y2, flag, string, x3, y3);
		break;
	}
	case WM_PAINT:
	{
		OnWmPaint(hWnd, wParam, lParam);
	}
	case WM_MDIACTIVATE:
	{
		CHILD_WND_DATA* wndDatatmp = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		GlobalColor = wndDatatmp->color;
		GlobalLFont = CreateFontIndirect(&wndDatatmp->hFont);
		hChildWnd = hWnd;
	}
	break;
	case WM_SIZE:
	{
		CHILD_WND_DATA * wndDatatmp = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		GlobalColor = wndDatatmp->color;
		GlobalLFont = CreateFontIndirect(&wndDatatmp->hFont);
	}
	break;
	case WM_MDIDESTROY:
		break;
	case WM_CLOSE:
		SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
		hChildWnd = NULL;
		break;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}

// Message handler for about box.
void initFrameWindow(HWND hWnd)
{
	hFrameWnd = hWnd;
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	ccs.idFirstChild = 50001;
	hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL, 0, 0, 0, 0, hWnd, (HMENU)NULL, hInst, (LPVOID)&ccs);
	ShowWindow(hwndMDIClient, SW_SHOW);
}
void onNewNonameWnd(HWND hWnd)
{
	wsprintf(szNonameTitle, L"Noname-%d.drw", count1);
	MDICREATESTRUCT mdiCreate;
	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));
	mdiCreate.szClass = szNonameWindowClass;
	mdiCreate.szTitle = szNonameTitle;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
	count1++;
}
void CreateToolBar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();
	TBBUTTON tbButtons[] =
	{
	// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
	{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	};

	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));
	TBBUTTON tbButtonsEdit[] = 
	{
	{ STD_CUT,	ID_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_COPY,	ID_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_PASTE, ID_EDIT_PASTE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_DELETE,ID_EDIT_DELETE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	};
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtonsEdit);
	TBBUTTON tbButtonsBonus[] =
	{
	{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 0, ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	TBADDBITMAP	tbBitmap = { hInst, IDB_LINE };
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);
	TBADDBITMAP	tbBitmap1 = { hInst, IDB_RECTANGLE };
	int idx1 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap1) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap1);
	TBADDBITMAP	tbBitmap2 = { hInst, IDB_ELLIPSE };
	int idx2 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap2) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap2);
	TBADDBITMAP	tbBitmap3 = { hInst, IDB_TEXT };
	int idx3 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap3) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap3);
	TBADDBITMAP	tbBitmap4 = { hInst, IDB_SELECTOBJECT };
	int idx4 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap4) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap4);
	tbButtonsBonus[0].iBitmap += idx;
	tbButtonsBonus[1].iBitmap += idx1;
	tbButtonsBonus[2].iBitmap += idx2;
	tbButtonsBonus[3].iBitmap += idx3;
	tbButtonsBonus[4].iBitmap += idx4;
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtonsBonus);
}
void onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int type;
	int wmID = LOWORD(wParam);
	switch (wmID)
	{
	case ID_DRAW_LINE:
	case ID_DRAW_RECTANGLE:
	case ID_DRAW_ELLIPSE:
	case ID_DRAW_TEXT:
	case ID_DRAW_SELECTOBJECT:
	{
		HMENU hSub = GetSubMenu(GetMenu(hWnd), 1);
		CheckMenuItem(hSub, currentitem, MF_UNCHECKED | MF_BYCOMMAND);
		CheckMenuItem(hSub, wmID, MF_CHECKED | MF_BYCOMMAND);
		currentitem = wmID;
	}
	break;
	case ID_FILE_NEW:
		onNewNonameWnd(hWnd);
		break;
	case ID_FILE_OPEN:
		if (hChildWnd == NULL)
			onNewNonameWnd(hWnd);
		OpenFile(hWnd);
		InvalidateRect(hChildWnd, NULL, true);
		break;
	case ID_FILE_SAVE:
		SaveFile(hWnd);
		break;
	case ID_FILE_EXIT:
		DestroyWindow(hWnd);
		break;
	case ID_EDIT_COPY:
		Copy(hWnd);
		InvalidateRect(hChildWnd, NULL, true);
		break;
	case ID_EDIT_PASTE:
		Paste(hWnd);
		InvalidateRect(hChildWnd, NULL, true);
		break;
	case ID_EDIT_CUT:
	{
		Copy(hWnd);
		Delete(hWnd);
		InvalidateRect(hChildWnd, NULL, true);
	}
		
		break;
	case ID_EDIT_DELETE:
		Delete(hWnd);
		break;
	case ID_DRAW_FONT:
	{
		CHOOSEFONT cf;
		LOGFONT lf;
		ZeroMemory(&cf, sizeof(CHOOSEFONT));
		cf.lStructSize = sizeof(CHOOSEFONT);
		cf.hwndOwner = hWnd;
		cf.lpLogFont = &lf;

		cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

		if (ChooseFont(&cf))
		{
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
			GlobalLFont = CreateFontIndirect(&lf);
			GetObject(GlobalLFont, sizeof(wndData->hFont), &wndData->hFont);
			SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
		}
	}
	break;
	case ID_DRAW_COLOR:
	{
		CHOOSECOLOR cc;
		static COLORREF acrCustClr[16];
		static DWORD rgbCurrent;
		ZeroMemory(&cc, sizeof(cc));
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = hWnd;
		cc.lpCustColors = (LPDWORD)acrCustClr;
		cc.rgbResult = rgbCurrent;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT;
		if (ChooseColor(&cc) == TRUE)
		{
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
			GlobalColor = cc.rgbResult;
			wndData->color = GlobalColor;
			SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
		}
	}
	break;
	case ID_WINDOW_CASCADE:
		SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
		break;
	case ID_WINDOW_TIDE:
		SendMessage(hwndMDIClient, WM_MDITILE, 0, 0);
		break;
	case ID_WINDOW_CLOSEALL:
		EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
		break;
	}
}
LRESULT CALLBACK MDICloseProc(HWND hChildwnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildwnd, 0L);
	return 1;
}
void LINE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
}
void LINE::WriteFile(string filePath, ofstream &output)
{
	output.write((char*)(&type), sizeof(int));
	output.write((char*)(&color), sizeof(COLORREF));
	output.write((char*)&left, sizeof(int));
	output.write((char*)&top, sizeof(int));
	output.write((char*)&right, sizeof(int));
	output.write((char*)&bottom, sizeof(int));
	wsprintf(msg, L"Line: %d %d %d %d", left, top, right, bottom);
}

void RECTANGLE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void RECTANGLE::WriteFile(string filePath, ofstream &output)
{
	output.write((char*)(&type), sizeof(int));
	output.write((char*)(&color), sizeof(COLORREF));
	output.write((char*)(&left), sizeof(int));
	output.write((char*)(&top), sizeof(int));
	output.write((char*)(&right), sizeof(int));
	output.write((char*)(&bottom), sizeof(int));
	wsprintf(msg, L"Rect: %d %d %d %d", left, top, right, bottom);
}
void ELLIPSE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	Ellipse(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}
void ELLIPSE::WriteFile(string filePath, ofstream &output)
{
	output.write((char*)(&type), sizeof(int));
	output.write((char*)(&color), sizeof(COLORREF));
	output.write((char*)(&left), sizeof(int));
	output.write((char*)(&top), sizeof(int));
	output.write((char*)(&right), sizeof(int));
	output.write((char*)(&bottom), sizeof(int));
	wsprintf(msg, L"Ellipse: %d %d %d %d", left, top, right, bottom);
}
void TEXT::Draw(HDC hdc)
{
	HFONT hFont = CreateFontIndirect(&LFont);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	TextOut(hdc, left, top, str, wcsnlen(str, MAX_STRING));
	DeleteObject(hFont);
}

void TEXT::WriteFile(string filePath, ofstream &output)
{
	output.write((char*)(&type), sizeof(int));
	output.write((char*)(&color), sizeof(COLORREF));
	output.write((char*)(&left), sizeof(int));
	output.write((char*)(&top), sizeof(int));
	output.write((char*)(&right), sizeof(int));
	output.write((char*)(&bottom), sizeof(int));
	output.write((char*)(&LFont), sizeof(LOGFONT));
	output.write((char*)(&str), sizeof(str));
	wsprintf(msg, L"Text: %d %d %d %d ", left, top, right, bottom);
}
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2)
{
	if ((wParam&MK_LBUTTON) == MK_LBUTTON)
	{
		HDC hdc;
		hdc = GetDC(hWnd);
		HPEN hPen = CreatePen(PS_SOLID, 1, GlobalColor);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);
		MoveToEx(hdc, x1, y1, NULL);
		switch (currentitem)
		{
		case ID_DRAW_LINE:
			LineTo(hdc, x2, y2);
			break;
		case ID_DRAW_ELLIPSE:
			Ellipse(hdc, x1, y1, x2, y2);
			break;
		case ID_DRAW_RECTANGLE:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		default:
			break;
		}
		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);

		MoveToEx(hdc, x1, y1, NULL);
		switch (currentitem)
		{
		case ID_DRAW_LINE:
			LineTo(hdc, x2, y2);
			break;
		case ID_DRAW_ELLIPSE:
			Ellipse(hdc, x1, y1, x2, y2);

			break;
		case ID_DRAW_RECTANGLE:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		default:
			break;

		}
		ReleaseDC(hWnd, hdc);
	}
}
HWND InputText(HWND hWnd, int left, int top)
{
	HWND EditBox = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, left, top,
		500, 25, hWnd, nullptr, hInst, nullptr);
	ShowWindow(EditBox, SW_NORMAL);
	return EditBox;
}
void OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2)
{
	SetFocus(hWnd);
	x1 = x2 = LOWORD(lParam);
	y1 = y2 = HIWORD(lParam);
}
void InitialChildWnd(HWND hWnd, int cType)
{
	CHILD_WND_DATA *wndData;
	wndData = new CHILD_WND_DATA;
	wndData->color = RGB(0, 0, 0);
	ZeroMemory(&(wndData->hFont), sizeof(LOGFONT));
	wndData->hFont.lfHeight = 28;
	wndData->Saved = false;
	wndData->checkIndex = -1;
	wcscpy_s(wndData->hFont.lfFaceName, LF_FACESIZE, L"Arial");
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData))
		if (GetLastError() != 0)
			MessageBox(hWnd, L"Cannot init data pointer of window", L"Error", MB_OK);
}
void OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2, bool &flag, HWND &string, int &x3, int &y3)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	if (wndData == NULL)
		MessageBox(hWnd, L"Error while getting data of window", L"Error", MB_OK);
	switch (currentitem)
	{
	case ID_DRAW_LINE:
	{
		LINE * l = new LINE;
		l->type = 0;
		l->color = GlobalColor;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_RECTANGLE:
	{
		RECTANGLE * l = new RECTANGLE;
		l->type = 1;
		l->color = GlobalColor;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_ELLIPSE:
	{
		ELLIPSE * l = new ELLIPSE;
		l->type = 2;
		l->color = GlobalColor;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_TEXT:
	{
		TEXT * l = new TEXT;
		if (flag == false)
		{
			string = InputText(hWnd, x1, y1);
			flag = true;
			x3 = x1;
			y3 = y1;
		}
		else
		{
			ShowWindow(string, SW_HIDE);
			WCHAR msg[MAX_LOADSTRING];
			HDC hdc = GetDC(hWnd);
			SelectObject(hdc, GlobalLFont);
			SetTextColor(hdc, GlobalColor);
			GetWindowText(string, msg, MAX_LOADSTRING);
			TextOut(hdc, x3, y3, msg, wcsnlen(msg, MAX_STRING));
			SIZE size;
			GetTextExtentPoint32(hdc, msg, wcsnlen(msg,MAX_STRING), &size);
			DeleteObject(GlobalLFont);
			ReleaseDC(hWnd, hdc);
			flag = false;
			l->type = 3;
			l->color = wndData->color;
			l->left = x3;
			l->top = y3;
			l->right = x3 + size.cx;
			l->bottom = y3 + size.cy;
			l->LFont = wndData->hFont;
			wsprintf(l->str, msg);
			wndData->objects.push_back(l);
		}
		break;
	}
	default:
		break;

	}
	SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
}
void OnWmPaint(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	WCHAR msg[MAX_LOADSTRING];
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL)
		return;
	wsprintf(msg, L"CHILD_WND_DATA size %d\n", wndData->objects.size());
	for (int i = 0; i < wndData->objects.size(); i++)
		wndData->objects[i]->Draw(hdc);
	EndPaint(hWnd, &ps);
}
void OpenFile(HWND hWnd)
{
	OPENFILENAME ofn; // CTDL dùng cho dialog open
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("Draw File (*.drw)\0*.drw\0All Files (*.*)\0*.*\0");
	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd; // handle của window cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile; // chuỗi tên file trả về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	ofn.lpstrDefExt = L"drw";
	GetOpenFileName(&ofn);
	int n = 0;
	int type;

	ifstream input;
	input.open(ofn.lpstrFile, ios::binary | ios::in);
	if (input)
	{
		input.read((char*)&n, sizeof(int));
		CHILD_WND_DATA *wndDataInput = new CHILD_WND_DATA;
		input.read((char*)&wndDataInput->hFont, sizeof(LOGFONT));
		input.read((char*)&wndDataInput->color, sizeof(COLORREF));
		for (int i = 0; i < n; i++)
		{
			input.read((char*)&type, sizeof(int));
			WCHAR check[128];
			if (type == 0)
			{
				LINE*l = new LINE;
				input.read((char*)&l->color, sizeof(COLORREF));
				input.read((char*)&l->left, sizeof(int));
				input.read((char*)&l->top, sizeof(int));
				input.read((char*)&l->right, sizeof(int));
				input.read((char*)&l->bottom, sizeof(int));
				wsprintf(msg, L"Line: %d %d %d %d", l->left, l->top, l->right, l->bottom);
				wndDataInput->objects.push_back(l);
			}
			else if (type == 1)
			{
				RECTANGLE *l = new RECTANGLE;
				input.read((char*)&l->color, sizeof(COLORREF));
				input.read((char*)&l->left, sizeof(int));
				input.read((char*)&l->top, sizeof(int));
				input.read((char*)&l->right, sizeof(int));
				input.read((char*)&l->bottom, sizeof(int));
				wsprintf(msg, L"Rect: %d %d %d %d", l->left, l->top, l->right, l->bottom);
				wndDataInput->objects.push_back(l);
			}
			else if (type == 2)
			{
				ELLIPSE *l = new ELLIPSE;
				input.read((char*)&l->color, sizeof(COLORREF));
				input.read((char*)&l->left, sizeof(int));
				input.read((char*)&l->top, sizeof(int));
				input.read((char*)&l->right, sizeof(int));
				input.read((char*)&l->bottom, sizeof(int));
				wsprintf(msg, L"Ellipse: %d %d %d %d", l->left, l->top, l->right, l->bottom);
				wndDataInput->objects.push_back(l);
			}
			else
			{
				TEXT*l = new TEXT;
				input.read((char*)&l->color, sizeof(COLORREF));
				input.read((char*)&l->left, sizeof(int));
				input.read((char*)&l->top, sizeof(int));
				input.read((char*)&l->right, sizeof(int));
				input.read((char*)&l->bottom, sizeof(int));
				input.read((char*)&l->LFont, sizeof(LOGFONT));
				input.read((char*)&l->str, sizeof(l->str));
				wsprintf(msg, L"Text: %d %d %d %d", l->left, l->top, l->right, l->bottom);
				wndDataInput->objects.push_back(l);

			}
		}
		input.close();
		WCHAR * FileName = PathFindFileName(ofn.lpstrFile);
		SetWindowText(hChildWnd, FileName);
		SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndDataInput);
	}

}
void SaveFile(HWND hWnd)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	OPENFILENAME ofn; // CTDL dùng cho dialog open
	TCHAR szFile[256];
	GetWindowText(hChildWnd, szFile, sizeof(szFile));
	TCHAR szFilter[] = TEXT("Draw File (*.drw)\0*.drw\0All Files (*.*)\0*.*\0");
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd; // handle của window cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile; // chuỗi tên file trả về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	ofn.lpstrDefExt = L"drw";
	if (hChildWnd != NULL)
	{
		if (wndData->Saved == false)
		{
			if (GetSaveFileName(&ofn))
			{
				ofstream output;
				wstring ws(ofn.lpstrFile);
				string filename(ws.begin(), ws.end());
				output.open(filename, ios::binary | ios::out);
				int n = wndData->objects.size();
				output.write((char*)&n, sizeof(int));
				output.write((char*)&wndData->hFont, sizeof(LOGFONT));
				output.write((char*)&wndData->color, sizeof(COLORREF));
				for (int i = 0; i < wndData->objects.size(); i++)
					wndData->objects[i]->WriteFile(filename, output);
				output.close();
				WCHAR * FileName = PathFindFileName(ofn.lpstrFile);
				SetWindowText(hChildWnd, FileName);
				SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
			}
			wndData->Saved = true;
		}
		else
		{
			ofstream output;
			wstring ws(ofn.lpstrFile);
			string filename(ws.begin(), ws.end());
			output.open(filename, ios::binary | ios::out);
			int n = wndData->objects.size();
			output.write((char*)&n, sizeof(int));
			output.write((char*)&wndData->hFont, sizeof(LOGFONT));
			output.write((char*)&wndData->color, sizeof(COLORREF));
			for (int i = 0; i < wndData->objects.size(); i++)
				wndData->objects[i]->WriteFile(filename, output);
			output.close();
			SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
		}
	}
	else
		MessageBox(hWnd, L"Don't have any child windows", L"Error", MB_ICONWARNING);
}
void LINE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type) {
	case 0:
	{
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}break;
	case 1:
	{
		left = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 2:
	{
		right = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}	break;
	}
	InvalidateRect(hWnd, NULL, true);
}

bool LINE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;

	a = right - left;
	b = bottom - top;
	func = b * (clickpoint.x - left) - a * (clickpoint.y - top);

	if (func >= 0 && func <= 2000 || func <= 0 && func >= -2000)
		return true;
	return false;
}

void LINE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_SOLID, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, left - RADIUS, top - RADIUS, left + RADIUS, top + RADIUS);
	Ellipse(hdc, right - RADIUS, bottom - RADIUS, right + RADIUS, bottom + RADIUS);
}

int LINE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;
	rec.left = rec.right = left;
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 1;
	rec.left = rec.right = right;
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 2;
	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

int LINE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, r;
	double func;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = 10;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}
void RECTANGLE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type)
	{
	case 0:
	{
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}break;
	case 1:
	{
		left = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 2:
	{
		right = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 3:
	{
		left = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}break;
	case 4:
	{
		right = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}break;
	}
	InvalidateRect(hWnd, NULL, true);
}


int RECTANGLE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b;
	int r;
	double func;
	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = RADIUS;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}

bool RECTANGLE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);

	if (bottom < top)
	{
		if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y <= top && clickpoint.y >= bottom) || clickpoint.x <= left && clickpoint.x >= right && clickpoint.y <= top && clickpoint.y >= bottom)
			return true;
	}
	else
	{
		if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y >= top && clickpoint.y <= bottom) || clickpoint.x <= left && clickpoint.x >= right && clickpoint.y >= top && clickpoint.y <= bottom)
			return true;
	}
	return false;
}

void RECTANGLE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_DASHDOTDOT, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Rectangle(hdc, left, top, right, bottom);
}

int RECTANGLE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;

	rec.left = rec.right = left;
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 1;

	rec.left = rec.right = right;
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 2;

	rec.left = rec.right = left;
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 3;

	rec.left = rec.right = right;
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 4;

	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}
void ELLIPSE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type)
	{
	case 0: {
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}	break;
	case 1:	left = LOWORD(lParam);	break;
	case 2:	top = HIWORD(lParam);	break;
	case 3:	right = LOWORD(lParam);	break;
	case 4:	bottom = HIWORD(lParam); break;
	default: break;
	}
	InvalidateRect(hWnd, NULL, true);
}

int ELLIPSE::makeChange(HWND hWnd, LPARAM lParam)
{
	return GetResizeType(lParam);
}

int ELLIPSE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b;
	int r;
	double func;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = RADIUS;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2)/* || func >= pow(r, 2) + 10*/)
		return true;
	return false;
}

bool ELLIPSE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b;
	int r;
	double func;

	a = abs(left + ((right - left) / 2));
	b = abs(top + (bottom - top) / 2);
	r = abs((right - left) / 2);
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}

void ELLIPSE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen;

	pen = CreatePen(PS_DASHDOTDOT, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, abs(left + ((right - left) / 2)) - RADIUS, top - RADIUS, abs(left + ((right - left) / 2)) + RADIUS, top + RADIUS);
	Ellipse(hdc, abs(left + ((right - left) / 2)) - RADIUS, bottom - RADIUS, abs(left + ((right - left) / 2)) + RADIUS, bottom + RADIUS);
	Ellipse(hdc, left - RADIUS, abs(top + (bottom - top) / 2) - RADIUS, left + RADIUS, abs(top + (bottom - top) / 2) + RADIUS);
	Ellipse(hdc, right - RADIUS, abs(top + (bottom - top) / 2) - RADIUS, right + RADIUS, abs(top + (bottom - top) / 2) + RADIUS);
}

int ELLIPSE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;

	rec.left = rec.right = left;
	rec.top = rec.bottom = abs(top + (bottom - top) / 2);
	if (isValidResize(lParam, rec))
		return 1;

	rec.left = rec.right = abs(left + ((right - left) / 2));
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 2;

	rec.left = rec.right = right;
	rec.top = rec.bottom = abs(top + (bottom - top) / 2);
	if (isValidResize(lParam, rec))
		return 3;

	rec.left = rec.right = abs(left + ((right - left) / 2));
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 4;

	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}
void TEXT::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type)
	{
	case 0: {
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	} break;
	default:break;
	}
	InvalidateRect(hWnd, NULL, true);
}

int TEXT::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b;
	int r;
	double func;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = abs((rec.right - rec.left) / 2);
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2) || func >= pow(r, 2) + 10)
		return true;
	return false;
}

bool TEXT::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);

	if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y >= top && clickpoint.y <= bottom) ||
		clickpoint.x <= left && clickpoint.x >= right && clickpoint.y >= top && clickpoint.y <= bottom)
		return true;
	return false;
}

void TEXT::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_SOLID, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, left - RADIUS, top - RADIUS, left + RADIUS, top + RADIUS);
	Ellipse(hdc, left - RADIUS, bottom - RADIUS, left + RADIUS, bottom + RADIUS);
	Ellipse(hdc, right - RADIUS, top - RADIUS, right + RADIUS, top + RADIUS);
	Ellipse(hdc, right - RADIUS, bottom - RADIUS, right + RADIUS, bottom + RADIUS);
}

int TEXT::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

void _Edit_OnLButtonDown(HWND hWnd, LPARAM lParam, int &type)
{
	CHILD_WND_DATA*p = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (!p->objects.empty())
	{
		InvalidateRect(hWnd, NULL, true);
		for (int i = 0; i < p->objects.size(); i++)
		{
			if (p->objects.at(i) != NULL)
			{
				if (p->objects.at(i)->isSelect(lParam) == true)
				{
					p->objects.at(i)->selected(hWnd);
					p->currentIndex = i;
					p->checkIndex = i;
					type = p->objects.at(p->currentIndex)->GetResizeType(lParam);
					break;
				}
			}

		}
	}
		if (p->currentIndex != -1)
		{
			type = p->objects.at(p->currentIndex)->GetResizeType(lParam);
			if (type == -1)
			{
				RECT rec;
				GetClientRect(hWnd, &rec);
				InvalidateRect(hWnd, &rec, true);
				for (int i = 0; i < p->objects.size(); i++)
				{
					if (p->objects.at(i) != NULL)
					{
						if (p->objects.at(i)->isSelect(lParam) == true)
						{
							p->objects.at(i)->selected(hWnd);
							p->currentIndex = i;
							p->checkIndex = i;
							type = p->objects.at(i)->GetResizeType(lParam);
							return;
						}
					}
				}
			}
		}
}
void _Edit_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, OBJECT*obj, int type)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	if (wndData->currentIndex != -1 && !wndData->objects.empty())
		obj->Resize(hWnd, lParam, wParam, type);
}
void Delete(HWND hWnd) 
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	if (wndData->currentIndex != -1) 
	{
		if (wndData)
			if (wndData->objects.size() != 0) 
			{
				vector<OBJECT*> objects;
				for (int i = 0; i < wndData->objects.size(); i++)
				{
					wsprintf(msg, L"%d", wndData->objects.size());
					OutputDebugString(msg);
					if (i == wndData->currentIndex)
						delete[]wndData->objects[i];
					else
					{
						objects.push_back(wndData->objects[i]);
					}
				}
				wndData->objects = objects;
				wndData->currentIndex = -1;
				SetWindowLongPtr(hChildWnd, 0, (LONG_PTR)wndData);
				InvalidateRect(hChildWnd, NULL, true);
			}
	}
}
void Copy(HWND hWnd)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	if (wndData->currentIndex != -1)
	{
		if (wndData)
		{
			if (OpenClipboard(hWnd))
			{
				EmptyClipboard();
				if (wndData->objects[wndData->currentIndex]->type == 0)
				{
					int nLineFormatID = RegisterClipboardFormat(szLine);
					HGLOBAL Line = GlobalAlloc(GHND, sizeof(LINE));
					LINE *l = (LINE *)GlobalLock(Line);
					l->type = wndData->objects[wndData->currentIndex]->type;
					l->color = wndData->objects[wndData->currentIndex]->color;
					l->left = wndData->objects[wndData->currentIndex]->left;
					l->top = wndData->objects[wndData->currentIndex]->top;
					l->right = wndData->objects[wndData->currentIndex]->right;
					l->bottom = wndData->objects[wndData->currentIndex]->bottom;
					GlobalUnlock(Line);
					SetClipboardData(nLineFormatID, Line);
				}
				else if (wndData->objects[wndData->currentIndex]->type == 1)
				{
					int nRectangleFormatID = RegisterClipboardFormat(szRectangle);
					HGLOBAL Rectangle = GlobalAlloc(GHND, sizeof(RECTANGLE));
					RECTANGLE *l = (RECTANGLE *)GlobalLock(Rectangle);
					l->type = wndData->objects[wndData->currentIndex]->type;
					l->color = wndData->objects[wndData->currentIndex]->color;
					l->left = wndData->objects[wndData->currentIndex]->left;
					l->right = wndData->objects[wndData->currentIndex]->right;
					l->top = wndData->objects[wndData->currentIndex]->top;
					l->bottom = wndData->objects[wndData->currentIndex]->bottom;
					GlobalUnlock(Rectangle);
					SetClipboardData(nRectangleFormatID, Rectangle);
				}
				else if (wndData->objects[wndData->currentIndex]->type == 2)
				{
					int nEllipseFormatID = RegisterClipboardFormat(szEllipse);
					HGLOBAL Ellipse = GlobalAlloc(GHND, sizeof(ELLIPSE));
					ELLIPSE *l = (ELLIPSE *)GlobalLock(Ellipse);
					l->type = wndData->objects[wndData->currentIndex]->type;
					l->color = wndData->objects[wndData->currentIndex]->color;
					l->left = wndData->objects[wndData->currentIndex]->left;
					l->right = wndData->objects[wndData->currentIndex]->right;
					l->top = wndData->objects[wndData->currentIndex]->top;
					l->bottom = wndData->objects[wndData->currentIndex]->bottom;;
					GlobalUnlock(Ellipse);
					SetClipboardData(nEllipseFormatID, Ellipse);
				}
				else if (wndData->objects[wndData->currentIndex]->type == 3)
				{
					TEXT* copy = (TEXT*)wndData->objects[wndData->currentIndex];
					int nTextFormatID = RegisterClipboardFormat(szText);
					HGLOBAL Text = GlobalAlloc(GHND, sizeof(TEXT));
					TEXT *l = (TEXT *)GlobalLock(Text);
					l->type = copy->type;
					l->color = copy->color;
					l->left = copy->left;
					l->right = copy->right;
					l->top = copy->top;
					l->bottom = copy->bottom;
					l->LFont = copy->LFont;
					wsprintf(l->str, copy->str);
					GlobalUnlock(Text);
					SetClipboardData(nTextFormatID, Text);
				}
			}
			CloseClipboard();
		}
			
	}
}
void Paste(HWND hWnd)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hChildWnd, 0);
	if (wndData->currentIndex != -1 && wndData->checkIndex != -1);
	{
		if (wndData)
		{
			if (OpenClipboard(hWnd))
			{
				int nLineFormatID = RegisterClipboardFormat(szLine);
				HGLOBAL Line = GetClipboardData(nLineFormatID);
				int nRectangleFormatID = RegisterClipboardFormat(szRectangle);
				HGLOBAL Rectangle = GetClipboardData(nRectangleFormatID);
				int nEllipseFormatID = RegisterClipboardFormat(szEllipse);
				HGLOBAL Ellipse = GetClipboardData(nEllipseFormatID);
				int nTextFormatID = RegisterClipboardFormat(szText);
				HGLOBAL Text = GetClipboardData(nTextFormatID);
				HGLOBAL CFText = GetClipboardData(CF_TEXT);
				if (Line)
				{
					LINE *l = (LINE *)GlobalLock(Line);
					LINE *storage = new LINE;
					storage->type = l->type;
					storage->color = l->color;
					storage->top = l->top;
					storage->left = l->left;
					storage->right = l->right;
					storage->bottom = l->bottom;
					wndData->objects.push_back(storage);
					InvalidateRect(hChildWnd, NULL, true);
				}
				else if (Rectangle)
				{
					RECTANGLE *l = (RECTANGLE *)GlobalLock(Rectangle);
					RECTANGLE *storage = new RECTANGLE;
					storage->type = l->type;
					storage->color = l->color;
					storage->top = l->top;
					storage->left = l->left;
					storage->right = l->right;
					storage->bottom = l->bottom;
					wndData->objects.push_back(storage);
					InvalidateRect(hChildWnd, NULL, true);
				}
				else if (Ellipse)
				{
					ELLIPSE *l = (ELLIPSE *)GlobalLock(Ellipse);
					ELLIPSE *storage = new ELLIPSE;
					storage->type = l->type;
					storage->color = l->color;
					storage->top = l->top;
					storage->left = l->left;
					storage->right = l->right;
					storage->bottom = l->bottom;
					wndData->objects.push_back(storage);
					InvalidateRect(hChildWnd, NULL, true);
				}
				else if (Text)
				{
					TEXT *l = (TEXT *)GlobalLock(Text);
					TEXT *storage = new TEXT;
					storage->type = l->type;
					storage->color = l->color;
					storage->top = l->top;
					storage->left = l->left;
					storage->right = l->right;
					storage->bottom = l->bottom;
					storage->LFont = l->LFont;
					wsprintf(storage->str, l->str);
					wndData->objects.push_back(storage);
					InvalidateRect(hChildWnd, NULL, true);
				}
				else if (CFText)
				{
					char* s = (char*)GlobalLock(CFText);
					TEXT * CFText = new TEXT;
					CFText->type = 3;
					CFText->color = wndData->color;
					CFText->left = 0;
					CFText->top = 0;
					mbstowcs(CFText->str, s, MAX_STRING);
					HDC hdc = GetDC(hChildWnd);
					HFONT hFont = CreateFontIndirect(&wndData->hFont);
					SelectObject(hdc, hFont);
					SetTextColor(hdc, wndData->color);
					TextOut(hdc, CFText->left, CFText->top, CFText->str, wcsnlen(CFText->str, MAX_STRING));
					SIZE size;
					GetTextExtentPoint32(hdc, CFText->str, wcsnlen(CFText->str, MAX_STRING), &size);
					ReleaseDC(hChildWnd, hdc);
					DeleteObject(hFont);
					wsprintf(msg, L"%d %d ", size.cx, size.cy);
					OutputDebugString(msg);
					CFText->right = size.cx;
					CFText->bottom = size.cy;
					CFText->LFont = wndData->hFont;
					wndData->objects.push_back(CFText);
					InvalidateRect(hChildWnd, NULL, true);

				}
				GlobalUnlock(Line);
				GlobalUnlock(Rectangle);
				GlobalUnlock(Ellipse);
				GlobalUnlock(Text);
				GlobalUnlock(CFText);
			}
			CloseClipboard();
		}
			
	}
}
