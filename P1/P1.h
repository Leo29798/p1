#pragma once
#include <commdlg.h>
#include <commctrl.h>	
#include <fstream>
#include <vector>
#include "shlwapi.h"
#define MAX_STRING		128
#define MAX_LOADSTRING	100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH		18
#define IMAGE_HEIGHT    18
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32
#define RADIUS 5
#define PEN_COLOR RGB(0, 0, 0)

using namespace std;
// Global Variables:
class OBJECT
{
public:
	int x_current;
	int y_current;
	int left;
	int top;
	int right;
	int bottom;
	int type;
	COLORREF color;
	HBRUSH hBrush;
	virtual void Draw(HDC hdc) = 0;
	virtual void WriteFile(string filePath, ofstream &output) = 0;
	virtual void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc) = 0;
	virtual int isValidResize(LPARAM lParam, RECT rec) = 0;
	virtual bool isSelect(LPARAM lParam) = 0;
	virtual void selected(HWND hWnd) = 0;
	virtual int GetResizeType(LPARAM lParam) = 0;
};
class LINE :public OBJECT
{
public:
	void Draw(HDC hdc);
	void WriteFile(string filePath, ofstream &output);
	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class RECTANGLE :public OBJECT
{
public:
	void Draw(HDC hdc);
	void WriteFile(string filePath, ofstream &output);
	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class ELLIPSE :public OBJECT
{
public:
	void Draw(HDC hdc);
	void WriteFile(string filePath, ofstream &output);
	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int makeChange(HWND hWnd, LPARAM lParam);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class TEXT :public OBJECT
{
public:
	WCHAR str[MAX_STRING];
	LOGFONT LFont;
	void Draw(HDC hdc);
	void WriteFile(string filePath, ofstream &output);
	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class CHILD_WND_DATA
{
public:
	LOGFONT hFont;
	COLORREF color;
	bool Saved;
	int currentIndex;
	vector<OBJECT*> objects;
	int checkIndex;
};
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szNonameTitle[MAX_LOADSTRING];
WCHAR szNonameWindowClass[MAX_LOADSTRING];
WCHAR szLine[20] = L"Line";
WCHAR szEllipse[20] = L"Ellipse";
WCHAR szRectangle[20] = L"Rectangle";
WCHAR szText[20] = L"Text";
WCHAR msg[128];
HWND hwndMDIClient = NULL;
HWND hFrameWnd = NULL;
HWND  hToolBarWnd;
HWND hChildWnd = NULL;
int count1 = 1;
int currentitem = 0;
int type1 = -1;
COLORREF GlobalColor = RGB(0, 0, 0);
HBRUSH GlobalHBrush = CreateSolidBrush(RGB(255, 255, 255));
HFONT GlobalLFont;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void onCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK MDICloseProc(HWND hChildwnd, LPARAM lParam);
LRESULT CALLBACK NonameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void initFrameWindow(HWND hWnd);
void CreateToolBar(HWND hWnd);
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2);
HWND InputText(HWND hWnd, int left, int top);
void OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2);
void InitialChildWnd(HWND hWnd, int ctype);
void OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam, int &x1, int &x2, int &y1, int &y2, bool &inputted, HWND &hText, int &x_saved, int &y_saved);
void OnWmPaint(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OpenFile(HWND hWnd);
void SaveFile(HWND hWnd);
void _Edit_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, OBJECT*obj, int type);
void _Edit_OnLButtonDown(HWND hWnd, LPARAM lParam, int &type);
void Delete(HWND hWnd);
void Copy(HWND hWnd);
void Paste(HWND hWnd);
#include "resource.h"
